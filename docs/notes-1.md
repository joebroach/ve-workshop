Notes from pt 1

# Design Considerations
Basic structure
  1. pop. synthesis
  2. travel sim
  3. system eval

## Geographies
Zones are flexible, user-defined, though practical limits to size.
  * Region = MArea: fwy lane miles (HPMS), arterial lane miles (HPMS), transit total supply
  * AZones: PUMS (pop by age + GQ)
  * Bzones: housing by loctype + employment; 4 Ds (incl. transit 1/4 mi)
      * EPA Smart Location used (but could be swapped)

# Five Key Concepts

  Core
  1. HH synthesis: hh by loctype + employment
  2. HH multimodal travel: hh VMT (2001 NHTS), Short trip SOV diversion (bikes, etc.) by age/hh characteristics
  3. Veh/Fuel/Emissions: veh ownership + type (incl. budget modeling); treat commercial/heavy truck as fleets  

  Adjustments
  4. Congestion: baseyear VMT + Growth by vehtype by factype; BPR curves + congestion fees drive re-balancing of VMT (all at **system** level; no network); feeds into fuel-speed curves
  5. HH Costs/Budgets

# Running the models

  * Setup for an area takes about 6 mos
  * Each run wipes output, so should separate runs by directory
