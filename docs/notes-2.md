Notes from part 2

# Ideology
Entire module expressed in single script (!)

# Nuts and Bolts
Basic dev steps
  1. Development relies on Rtools (currently v3.5)
  2. clone VE-dev (or VE)
  3. clone VE-installer
  4. run VE-Installer make - gathers dependencies, including runtime folder (allows testing your code via batch script)
BG: fork > clone > branch > code > ??

New module built first as package (mostly for testing), then installed into ve-lib.

Important dev files
*VE-config.yml*
*VE_components.yml*
